package fp

/**
  * Created by shiv on 8/7/16.
  */
object Ch2 {

  def isSorted[A](as: Array[A], ordered: (A, A) => Boolean): Boolean = {
    def loop(n: Int): Boolean = {
      if (n + 1 >= as.size) true
      else if (!ordered(as(n), as(n + 1))) false
      else loop(n + 1)

    }

    loop(0)
  }

  def curry[A, B, C](f: (A, B) => C): A => B => C = (a: A) => (b: B) => f(a, b)

  def uncurry[A, B, C](f: A => B => C): (A, B) => C = (a: A, b: B) => f(a)(b)

  def compose[A, B, C](f: A => B, g: B => C): A => C = (a: A) => g(f(a))

  def main(args: Array[String]): Unit = {
    val curriedSum = curry((x: Int, y: Int) => x + y)
    val partial2Add = curriedSum(2);
    val x = partial2Add(3)
    val y = partial2Add(5)
    println(x)
    println(y)

    val isOrdered: (Int, Int) => Boolean = (x: Int, y: Int) => y >= x
    var testsort = isSorted(Array(1, 2, 3, 4, 5), isOrdered)

    println(testsort)

    testsort = isSorted(Array(1, 3, 9, 4, 5), isOrdered)

    println(testsort)

  }
}
